from flask import Flask
from flask import render_template, request, jsonify
from decimal import Decimal

app = Flask(__name__)

@app.route("/", methods=['GET','POST'])
def index():
    #query string
    product_obj = {}
    if request.method == 'GET':
        product = request.args.get('product')
        price = request.args.get('price')
        qty = request.args.get("qty")
        product_obj["product"] = product
        product_obj["price"] = price and Decimal(price)
        product_obj["qty"] = qty and Decimal(qty)
        print(f"{type(product_obj)}")
        print(product_obj)
    if request.method == 'POST':
        product = request.form.get('product')
        price = request.form.get('price')
        qty = request.form.get('qty')
        print(f"{product}")
        product_obj['product'] = product
        product_obj['price'] = price and Decimal(price)
        product_obj['qty'] = qty and Decimal(qty)
        print(f"product: {product_obj['product']}\nprice: {product_obj['price']}\nqty: {product_obj['qty']}\ntotal: {product_obj['price'] * product_obj['qty']}")
        # return jsonify({'result':'ok'})
    return render_template("ajax.html", product_obj=product_obj)