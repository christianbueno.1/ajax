# Ajax
## XMLHttpRequest

    let xhr = new XMLHttpRequest();

* methods: xhr.send(data), xhr.responseText

Reading text files
```
<input type="file" name="form2-file" id="text-file-form2" accept="text/plain">
```
From JavaScript
```
$textFileForm2 = document.getElementById("text-file-form2");
$textFileForm2.files[0];
```
File object, text() method return a promise

    $textFileForm2.files[0].text();

* FileReader()
* Event: loadstart, progress, load, loadend, 